![logo](static/img/logo.png)

# Estoque

Documento de instalação do projeto Estoque.

solução para o desafio [querotrabalharnawpensar](https://bitbucket.org/wpensar/querotrabalharnawpensar)

**Requisitos:**

*   virtualenv
*   virtualenvwrapper


#### Instalação

* virtualenv

        $ mkdir estoque && cd estoque
        $ virtualenv env
        $ source env/bin/activate
        $ git clone https://caputo@bitbucket.org/caputo/querotrabalharnawpensar.git
        $ cd querotrabalharnawpensar/estoque
        $ pip install -r requirements.txt
        $ ./runserver.py
        * Running on http://127.0.0.1:8000/ (Press CTRL+C to quit)
        * Restarting with stat

* virtualenvwrapper

        $ mkproject querotrabalharnawpensar
        $ git clone https://caputo@bitbucket.org/caputo/querotrabalharnawpensar.git
        $ cd querotrabalharnawpensar/estoque
        $ pip install -r requirements.txt
        $ ./runserver.py
        * Running on http://127.0.0.1:8000/ (Press CTRL+C to quit)
        * Restarting with stat
 
#### Login

> Usuário: ``admin``
> Senha: ``admin``

## TODO

* ~~``Cadastro de produtos (Nome)``~~
* ~~``Compra de produtos (Produto, quantidade e preço de compra)``~~
* ~~``Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)``~~
* ~~``Exibição da fila de processamento das tarefas de calculo de preço médio (Aguardando, Em execução, Executados)``~~
* ~~``Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)``~~
* ~~``Ser WEB``~~
* ~~``Ser escrita em Python 2.7+``~~
* ~~``Só deve utilizar biliotecas livres e gratuitas``~~
* Melhorar aplicação utilizando micro-serviços com docker, rest, celery, redis, e rabbitmq.
* Migrar de ``peewee`` para ``sqlalchemy`` 

> Esse sistema não precisa ter, mas será um plus:

> * ~~Autenticação e autorização~~ ``(se for com OAuth, melhor ainda)``
> * ~~``Ter um design bonito``~~
> * Testes automatizados

> **TODO**:
>
>    * Adicionar Registro de Usuário
>    * Incluir Autenticação OAuth
>    * Preparar Testes Automatizados

