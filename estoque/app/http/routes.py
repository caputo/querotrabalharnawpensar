# -*- coding:utf8 -*-

from functools import wraps

from flask import session, request
from flask import Flask, render_template, redirect
from flask_bootstrap import *

from ..models.estoque import *

app = Flask('Estoque Online')
Bootstrap(app)


def logged_in(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('usuario') is not None:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))

    return decorated_function


def check_login(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('usuario') is None:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('admin'))

    return decorated_function


@app.before_request
def before_request():
    database.connect()


@app.after_request
def after_request(response):
    database.close()
    return response


@app.route('/')
@check_login
def index():
    return render_template('home.html')


# Página de Login e Registros
@app.route('/login', methods=['POST', 'GET'])
@check_login
def login():
    error = None
    if request.method == 'POST':
        try:
            user = Usuario.get(Usuario.login == request.form['user'])
        except:
            error = True
            return redirect(url_for('login'))

        if request.form['user'] == user.login and request.form['user-pass'] == user.senha:
            session['usuario'] = request.form['user']

            return redirect(url_for('admin'))
        else:
            error = True
    return render_template('login.html', **locals())


@app.route('/logout')
@logged_in
def logout():
    session.pop('usuario', None)
    return redirect(url_for('index'))


@app.route('/admin')
@logged_in
def admin():
    if 'usuario' in session:
        compra = Compra.select(
            Produto.nome,
            Compra.id,
            Compra.qtd_produto,
            Compra.vlr_compra,
            Compra.vlr_medio,
            Compra.status
        ).join(Produto)
        return render_template('admin/index.html', compra=compra)


@app.route('/addProduct', methods=['POST', 'GET'])
@logged_in
def add_product():
    sucesso = None
    error = None

    if request.method == 'POST':
        nProduto = request.form['nomeProduto']
        try:
            produto = Produto.create(
                nome=nProduto,
            )
            produto.save()
            sucesso = 'Produto cadastrado com sucesso!'
        except IntegrityError:
            error = ' Produto já cadastrado!'
    else:
        error = ' Cadastre um novo produto'
    return render_template('admin/addProduct.html', **locals())


@app.route('/purchaseProduct', methods=['POST', 'GET'])
@logged_in
def purchase_product():
    sucesso = None
    error = None
    produtos = Produto.select()

    if request.method == 'POST':
        nProduto = request.form.get('productId')
        vlrCompra = request.form['vlrCompra']
        qtdProduto = request.form['qtdProduto']

        try:
            compra = Compra.create(
                produto=nProduto,
                vlr_compra=float(vlrCompra),
                qtd_produto=qtdProduto,
                status=1
            )
            compra.save()
            sucesso = 'Compra realizada com sucesso!'
        except IntegrityError:
            error = 'Para realizar esta operação, cadastre um produto!'
    else:
        error = 'Finalize sua compra.'
    return render_template('admin/purchaseProduct.html', **locals())


@app.route('/purchaseProcessing/<int:id>')
def purchase_processing(id=None):
    if id is not None:
        query = Compra.update(vlr_medio=(Compra.vlr_compra / Compra.qtd_produto), status=3).where(Compra.id == id)
        query.execute()
        return redirect(url_for('admin'))


@app.route('/processingAllPurchases')
@logged_in
def Processing_all_purchases(id=None):
    query = Compra.update(vlr_medio=(Compra.vlr_compra / Compra.qtd_produto), status=3)
    query.execute()
    return redirect(url_for('admin'))


@app.route('/addNotes/<int:id>', methods=['POST', 'GET'])
@logged_in
def addNotes(id=None):
    if id is not None:
        compra = Compra.select(Produto.nome, Compra.id, Compra.qtd_produto, Compra.vlr_compra, Compra.notas,
                               Compra.status).join(
            Produto).where(Compra.id == id)
        if request.method == 'POST':
            vlrCompra = request.form['vlrCompra']
            qtdProduto = request.form['qtdProduto']
            editNotas = request.form['editNotas']

            query = Compra.update(
                vlr_compra=vlrCompra,
                qtd_produto=qtdProduto,
                notas=editNotas,
                status=2
            ).where(Compra.id == id)

            query.execute()

            return redirect(url_for('admin'))
    return render_template('admin/addNotes.html', **locals())


# Search
@app.route('/search', methods=['GET', 'POST'])
@logged_in
def search():
    if request.method == "POST":
        procurar = request.form['procurar']
        produto = Compra.select(
            Produto.nome,
            Compra.id,
            Compra.qtd_produto,
            Compra.vlr_compra,
            Compra.vlr_medio,
            Compra.status).join(
            Produto
        ).where(Produto.nome == procurar)
    return render_template('admin/search.html', **locals())
