# -*- coding:utf8 -*-

from peewee import *

database = SqliteDatabase('app/models/estoque.db', threadlocals=True)


class BaseModel(Model):
    class Meta:
        database = database


class Usuario(BaseModel):
    login = CharField(unique=True)
    senha = CharField()

    class Meta:
        order_by = ('login',)


class Produto(BaseModel):
    nome = CharField(unique=True)

    class Meta:
        order_by = ('nome',)


class StatusCompra(BaseModel):
    status = CharField(unique=True)

    class Meta:
        order_by = ('status',)


class Compra(BaseModel):
    produto = ForeignKeyField(Produto)
    status = ForeignKeyField(StatusCompra)
    qtd_produto = IntegerField()
    vlr_compra = DoubleField()
    vlr_medio = DoubleField(null=True)
    notas = TextField(null=True)

    class Meta:
        order_by = ('status',)


def create_tables():
    database.connect()
    database.create_tables([Usuario, Produto, Compra, StatusCompra])

    usuario = Usuario.create(
        login='admin',
        senha='admin'
    )

    for satus in ['Aguardando', 'Em execução', 'Executados']:
        statusCompra = StatusCompra.create(
            status=satus,
        )
        statusCompra.save()

    database.close()