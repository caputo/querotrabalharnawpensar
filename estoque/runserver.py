#!/usr/bin/env python
# -*- coding:utf8 -*-

import sys

reload(sys)
sys.setdefaultencoding("utf-8")

""" Executando a aplicação
    Debug True = Mostra erros da aplicação
    Host = Endereço de IP do qual você quer executar a aplicação
    Port = Porta da aplicação
"""
if __name__ == "__main__":

    import os

    from app.http.routes import *
    from app.models.estoque import *

    if not os.path.isfile('app/models/estoque.db'):
        create_tables()

    app.secret_key = '<secret_key_27>'
    app.run(debug=True, host='127.0.0.1', port=8000)
