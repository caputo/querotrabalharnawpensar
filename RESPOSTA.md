# **Porque trabalhar no Grupo WP?** #

![light](olaequipe/Assets/lightbulbs.gif)

* Porque quero trabalhar num ambiente MERITOCRÁTICO. Aqui os melhores são reconhecidos sempre!
* Porque quero aprender coisas novas a cada minuto.
* Porque quero ajudar a construir a maior empresa de tecnologia para educação do Brasil.
* Porque tenho sede por novos desafios.
* Porque quero fazer diferença!


# **Como faço para me candidatar?** #
1. ~~``Faça um fork do repositório``~~
2. ~~``Desenvolva o desafio de acordo com o proposto abaixo``~~
3. ~~``Mande um pull request com o curriculo e a resposta do desafio``~~

> [https://bitbucket.org/caputo/querotrabalharnawpensar](https://bitbucket.org/caputo/querotrabalharnawpensar)

## **Caso você não deseje que o envio seja público** ##
1. Faça um clone do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Envie um email com o curriculo e um arquivo patch para rh@wpensar.com.br


# **Desafio:** #
O conceito desse desafio é nos ajudar a avaliar as habilidades dos candidatos às vagas de backend.

Você tem que desenvolver um sistema de estoque para um supermercado.

Esse supermercado assume que sempre que ele compra uma nova leva de produtos, ele tem que calcular o preço médio de compra de cada produto para estipular um preço de venda. Para fins de simplificação assuma que produtos que tenham nomes iguais, são o mesmo produto e que não existe nem retirada e nem venda de produtos no sistema.

O cálculo do preço médio desse produto deve ser feito assincronamente e o valor calculado deve ser armazenado.

Seu sistema deve:

1. ~~``Cadastro de produtos (Nome)``~~

![Cadastro de Produto](olaequipe/Assets/cadastro_de_produto.png)

2. ~~``Compra de produtos (Produto, quantidade e preço de compra)``~~

![Compra de Produto](olaequipe/Assets/compra_de_produto.png)

3. ~~``Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)``~~

![Lista de Produtos Comprados](olaequipe/Assets/lista_de_produtos_comprados.png)

4. ~~``Exibição da fila de processamento das tarefas de calculo de preço médio (Aguardando, Em execução, Executados)``~~

![Fila de Processamento](olaequipe/Assets/fila_de_processamento.png)

5. ~~``Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)``~~

    * virtualenv

            $ mkdir estoque && cd estoque
            $ virtualenv env
            $ source env/bin/activate
            $ git clone https://caputo@bitbucket.org/caputo/querotrabalharnawpensar.git
            $ cd querotrabalharnawpensar/estoque
            $ pip install -r requirements.txt
            $ ./runserver.py

    * virtualenvwrapper

            $ mkproject querotrabalharnawpensar
            $ git clone https://caputo@bitbucket.org/caputo/querotrabalharnawpensar.git
            $ cd querotrabalharnawpensar/estoque
            $ pip install -r requirements.txt
            $ ./runserver.py

6. ~~``Ser WEB``~~

    ![Flask](olaequipe/Assets/flask.png)

7. ~~``Ser escrita em Python 2.7+``~~

        $ python --version
        Python 2.7.10

8. ~~``Só deve utilizar biliotecas livres e gratuitas``~~

        $ pip freeze
        dominate==2.1.16
        Flask==0.10.1
        Flask-Bootstrap==3.3.5.7
        itsdangerous==0.24
        Jinja2==2.8
        MarkupSafe==0.23
        peewee==2.6.4
        visitor==0.1.2
        Werkzeug==0.10.4
        wheel==0.24.0

  * **Licenças**

    [dominate - GNU Lesser General Public License v3 (LGPLv3)](https://raw.githubusercontent.com/Knio/dominate/master/LICENSE.txt) <= github

    [Flask - BSD License](https://raw.githubusercontent.com/mitsuhiko/flask/master/LICENSE) <= github

    [Flask-Bootstrap - BSD License](https://raw.githubusercontent.com/mbr/flask-bootstrap/master/LICENSE) <= github

    [itsdangerous - BSD License](https://raw.githubusercontent.com/mitsuhiko/itsdangerous/master/LICENSE) <= github

    [Jinja2 - BSD License](https://raw.githubusercontent.com/mitsuhiko/jinja2/master/LICENSE) <= github

    [MarkupSafe - BSD License](https://raw.githubusercontent.com/mitsuhiko/markupsafe/master/LICENSE) <= github

    [peewee - MIT License](https://raw.githubusercontent.com/coleifer/peewee/master/LICENSE) <= github

    [visitor - MIT](https://raw.githubusercontent.com/mbr/visitor/master/LICENSE) <= github

    [Werkzeug - BSD License](https://raw.githubusercontent.com/mitsuhiko/werkzeug/master/LICENSE) <= github

    [wheel - MIT](https://bitbucket.org/pypa/wheel/raw/1cb7374c9ea4d5c82992f1d9b24cf7168ca87707/LICENSE.txt) <= bitbucket


  * **Ferramentas/Biliotecas**


    [bower - MIT License](https://raw.githubusercontent.com/bower/bower/master/LICENSE) <= github

    [jquery - jQuery Foundation](https://raw.githubusercontent.com/jquery/jquery/master/LICENSE.txt) <= github


Para podermos visualizar a fila de processamento, assumindo que o calculo do preço médio é irrisorio computacionalmente, coloque algum delay na tarefa de calculo.

Esse sistema não precisa ter, mas será um plus:


1. ~~Autenticação e autorização~~ ``(se for com OAuth, melhor ainda)``
2. ~~``Ter um design bonito``~~
3. Testes automatizados

> **TODO**:
>
>    * Adicionar Registro de Usuário
>    * Incluir Autenticação OAuth
>    * Preparar Teste Automatizados

# **Avaliação:** #
![spok](olaequipe/Assets/spok.png)

Vamos avaliar seguindo os seguintes critérios:

1. Você conseguiu concluir os requisitos?
2. Você documentou a maneira de configurar o ambiente e rodar sua aplicação?


# **Condições e Beneficios Oferecidos:** #
* Local: No meio de Icarai, Niterói - RJ
* Regime CLT
* Período Integral
* Horário Flexível
* Vale Refeição
* Vale Transporte
* Ambiente descontraído
* Grande possibilidade de crescimento
* Trabalhar com projetos que mudam a Educação no País
* Café liberado
* Frutas liberadas
* Sinuca
* Cerveja
* PS3
* Tv a Cabo


## **Referência:** ##
Esse desafio foi baseado em outro desafio: https://github.com/myfreecomm/desafio-programacao-1

